---
title: Cum îmi schimb parola?
date: 2020-08-16
tags: [
    "securitate",
    "19.3",
    "cinnamon"
]
categories: [
    "cum",
]
---

## Problema

Vreau să îmi schimb parola.

## Soluția

Încearcă cei trei pași de mai jos.

### Pasul 1

Deschide meniul și scrie "cont".

Apar câteva programe care conțin "cont" în nume.

Alege "Detalii cont".

![Meniu, Detalii cont](/screenshot/meniu-detalii-cont.png)

### Pasul 2

În fereastra care apare, apasă pe câmpul "Parolă".

![...](/screenshot/schimba-parola-1.png)

### Pasul 3

Acum în primul câmp scrie parola ta de acum (Asta se face ca să fim siguri că nu îți schimbă pisica parola, până te duci la baie).

În al doilea câmp, scrie noua parolă pe care o dorești.

În al treilea câmp, mai scrie o dată, ca să fim siguri că nu ai tastat greșit.

Urmărește indicatorul de calitate a parolei, și caută să pui una care să fie considerată cât mai bună (dar pe care să o poți și ține minte).

O parolă bună are o lungime cât-de-cât (nu doar 2-4 litere), conține litere mari dar și mici, poate niște cifre,
poate niște caractere speciale (două puncte, semnul exclamării), nu seamănă cu numele tău și nu este un cuvânt din dicționar.

![...](/screenshot/schimba-parola-2.png)

La final, apasă "Schimbă". Celelalte ferestre le poți închide.
