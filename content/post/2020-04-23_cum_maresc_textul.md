---
title: Cum măresc textul?
date: 2020-04-23
tags: [
    "aspect",
    "19.3",
    "cinnamon"
]
categories: [
    "cum",
]
---

## Problema

Tot textul de pe ecran este prea mărunt.

## Soluția

Încearcă cei trei pași de mai jos.

### Pasul 1

Deschide meniul și du-te la "Setări sistem" (a 3-a de sus, pe stânga)

![Meniu, Setări sistem](/screenshot/meniu-setari-sistem.png)

### Pasul 2

Din fereastra de Setări, alege "Selecție fonturi"

![...](/screenshot/deschide-setari-fonturi.png)

### Pasul 3

Folosește butonul <key>+</key> de la "Factor de scalare a textului" ca să mărești fontul peste tot.

Apăsând o dată se face `1.1`, încă o dată - `1.2` și tot așa.

![...](/screenshot/factor-scalare-text.png)

Când arată cum vrei tu, închide fereastra. Nu trebuie să salvezi nimic.
